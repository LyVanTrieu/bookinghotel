﻿using BookingHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingHotel.ObjectView
{
    
    public class Thongtindangki
    {
        DBConnection db = new DBConnection();
        public List<Thongtin> listthongtin()
        {
            List<Thongtin> ltt = new List<Thongtin>();
            var donhang = db.PhieuDangKis.ToList();
            foreach (var item in donhang)
            {
                Thongtin tt = new Thongtin();
                tt.Hoten = item.KhachHang.tenKH;
                tt.SDT = item.KhachHang.Sdt;
                tt.Email = item.KhachHang.Email;
                tt.MaPhong = item.maPhong;
                tt.SoNgay =  item.Tongtien / item.Phong.giaPhong;
                tt.MaKH = item.maKH;
                tt.TrangThai = item.trangThai;
                tt.MaDK = item.maPDK;
                tt.Tongtien = item.Tongtien;
                tt.Xacnhan = item.xacNhan;
                ltt.Add(tt);
                
            }
            return ltt;
        }
    }
    public class Thongtin
    {
        public string Hoten { get; set; }
        public string SDT { get; set; }
        public string Email { get; set; }
        public string MaPhong { get; set; }
        public int? SoNgay { get; set; }
        public bool? TrangThai { get; set; }
        public bool? Xacnhan {get;set;}
        public int? MaKH { get; set; }
        public int MaDK { get; set; }
        public int? Tongtien { get; set; }
    }
}