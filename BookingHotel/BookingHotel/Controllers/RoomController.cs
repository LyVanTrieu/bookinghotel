﻿using BookingHotel.Models;
using BookingHotel.ObjectView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingHotel.Controllers
{
    public class RoomController : Controller
    {
        DBConnection db = new DBConnection();
        // GET: Room
        public ActionResult Index(String name, String cate)
        {
            String add = name;
            String cate1 = cate;
            viewroom Room = new viewroom();
            if (String.IsNullOrEmpty(name) && String.IsNullOrEmpty(cate))
            {
                Room.lstRoom = db.Phongs.Where(p =>p.Status ==true && p.LoaiPhong.PStatus == true).ToList();
            }
            if (String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(cate))
            {
                Room.lstRoom = db.Phongs.Where(p => p.maLoai.Equals(cate) && p.Status == true && p.LoaiPhong.PStatus == true).ToList();
            }
            if (!String.IsNullOrEmpty(name) && String.IsNullOrEmpty(cate))
            {
                Room.lstRoom = db.Phongs.Where(p => p.tenPhong.Contains(name) && p.Status == true && p.LoaiPhong.PStatus == true).ToList();
            }
            if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(cate))
            {
                Room.lstRoom = db.Phongs.Where(p => p.maLoai.Equals(cate) && p.tenPhong.Contains(name) && p.Status == true && p.LoaiPhong.PStatus == true).ToList();
            }
            Room.lstcateRoom = db.LoaiPhongs.ToList();
            return View(Room);
        }
        public ActionResult Search(FormCollection form)
        {
            var name = form["searchbox"];
            var cate = form.Get("searchcate");
            return RedirectToAction("index", new { name = name, cate = cate });
        }
        //detail room
        public ActionResult Detail(String id)
        {
            var roomDetail = from s in db.Phongs
                             where s.maPhong.Equals(id)
                             select s;
            return View(roomDetail.SingleOrDefault());
        }
    }
}