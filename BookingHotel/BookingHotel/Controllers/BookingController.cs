﻿using BookingHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingHotel.Controllers
{
    public class BookingController : Controller
    {
        DBConnection db = new DBConnection();
        // GET: Booking
        public ActionResult IndexBooking(String id)
        {
            var Phong = db.Phongs.FirstOrDefault(p => p.maPhong.Equals(id));
            return View(Phong);
        }
        [HttpPost]
        public ActionResult addcart(FormCollection form, string id)
        {
            var tenKH = form["tenKH"];
            var hoKH = form["hoKH"];
            var Email = form["Email"];
            var Sdt = form["Sdt"];
            var diaChi = form["diaChi"];
            var ngayNhan = form["ngayNhan"];
            var ngayTra = form["ngayTra"];
            var soNguoi = form["soNguoi"];
            var Tongtien = form["Tongtien"];
           
                KhachHang KH = new KhachHang();
                PhieuDangKi DK = new PhieuDangKi();
                if ((db.KhachHangs.FirstOrDefault(p => p.Sdt.Equals(Sdt))) == null)
                {

                    KH.tenKH = hoKH + " " + tenKH;
                    KH.Email = Email;
                    KH.Sdt = Sdt;
                    KH.diaChi = diaChi;
                    db.KhachHangs.Add(KH);
                    DK.maNV = null;
                    DK.ngayNhan = (DateTime.Parse(ngayNhan));
                    DK.ngayDi = (DateTime.Parse(ngayTra));
                    DK.soNguoi = soNguoi;
                    DK.Tongtien = (int.Parse(Tongtien.Replace("$", "")));
                    DK.ngayLap = DateTime.Today;
                    DK.maKH = KH.maKH;
                    DK.Email = Email;
                    DK.xacNhan = false;
                    DK.trangThai = false;
                    DK.maPhong = id;
                    db.PhieuDangKis.Add(DK);
                    
                    
                }
                else {
                    DK.maNV = null;
                    DK.ngayNhan = (DateTime.Parse(ngayNhan));
                    DK.ngayDi = (DateTime.Parse(ngayTra));
                    DK.soNguoi = soNguoi;
                    DK.Tongtien = (int.Parse(Tongtien.Replace("$", "")));
                    DK.ngayLap = DateTime.Today;
                    DK.maKH = db.KhachHangs.FirstOrDefault(p => p.Sdt.Equals(Sdt)).maKH;
                    DK.xacNhan = false;
                    DK.Email = Email;
                    DK.trangThai = false;
                    DK.maPhong = id;
                    db.PhieuDangKis.Add(DK);
                }
            
            db.SaveChanges();
            TempData["thanhtoan"] = "Đặt phòng thành công";
            return RedirectToAction("IndexBooking",new { id = id});
        }
        
    }


}