﻿using BookingHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingHotel.Controllers.Manager
{
    public class ProfileController : Controller
    {
        DBConnection db = new DBConnection();
        // GET: Profile
        public ActionResult profile()
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            NhanVien nv = Session["NV"] as NhanVien;
            return View(nv);
        }
        [HttpPost]
        public ActionResult profile(FormCollection form)//hàm cập nhật thông tin người dùng
        {
            NhanVien nv = Session["NV"] as NhanVien;
            DangNhap ac = Session["LoginAD"] as DangNhap;
            if (nv == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            //if (string.IsNullOrEmpty(form["email"]))
            //{
            //    ViewData["loiemail"] = "Email không được bỏ trống";
            //    ViewData["ketqua"] = "Cập nhật thất bại";
            //}
            else if (string.IsNullOrEmpty(form["phone"]))
            {
                ViewData["loiphone"] = "Số điện thoại không được bỏ trống";
                ViewData["ketqua"] = "Cập nhật thất bại";
            }
            else if (string.IsNullOrEmpty(form["address"]))
            {
                ViewData["loidc"] = "Địa chỉ không hợp lệ";
                ViewData["ketqua"] = "Cập nhật thất bại";
            }
            else if (!form["pass"].Equals(ac.passWord))
            {
                ViewData["loipass"] = "Mật khẩu không chính xác";
                ViewData["ketqua"] = "Cập nhật thất bại";
            }
            else
            {
                NhanVien update = db.NhanViens.FirstOrDefault(p => p.maNV == nv.maNV);
                //update.gm = form["email"];
                update.Sdt = form["phone"];
                update.diaChi = form["address"];
                db.SaveChanges();
                ViewData["ketqua"] = "Cập nhật thành công";
                Session["NV"] = update;
            }
            return profile();
        }
    }
}