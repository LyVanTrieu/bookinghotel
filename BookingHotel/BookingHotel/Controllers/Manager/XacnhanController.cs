﻿using BookingHotel.Models;
using BookingHotel.ObjectView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace BookingHotel.Controllers.Manager
{
    public class XacnhanController : Controller
    {
        DBConnection db = new DBConnection();
        // GET: Admin/Xacnhan
        
        public ActionResult Danhsachdatphong()
        {
            
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                Thongtindangki ttdk = new Thongtindangki();
                return View(ttdk.listthongtin().Where(p => p.Xacnhan == false).ToList());
            }
        }
        
        public ActionResult Thongke()
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                Thongtindangki ttdk = new Thongtindangki();
                return View(ttdk.listthongtin().Where(p => p.Xacnhan == true).ToList());
            }
            
        }

        public ActionResult xacnhan(int MaPhieu)
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                var message = new MailMessage();
                var PDK = db.PhieuDangKis.FirstOrDefault(p => p.maPDK == MaPhieu && p.xacNhan == false);
                if (PDK != null)
                {
                    PDK.xacNhan = true;

                    message.To.Add(new MailAddress(PDK.Email));
                    message.Subject = "Thông báo đăng ký phòng \t" + PDK.maPhong;
                    message.Body = "\n Cảm ơn bạn đã đặt phòng khách sạn T2D" + "  " + "\n\tngày nhận: " + PDK.ngayNhan + "  " + "\n\t ngày trả phòng: " + PDK.ngayDi + "   " + "\n\t tổng tiền phòng: " + PDK.Tongtien + "$" + "  " + "\n thân chào";
                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient())
                    {
                        smtp.Send(message);
                    }
                }
                else
                {
                    TempData["thongbao"] = "khong tim thay";
                }
                db.SaveChanges();
                //(PDK != null) ? PDK.xacNhan == true : TempData["thongbao"] = "khongtimthay";
                return RedirectToAction("Danhsachdatphong");
            }
            
        }

        public ActionResult thanhtoan(int MaPhieu)
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                var PDK = db.PhieuDangKis.FirstOrDefault(p => p.maPDK == MaPhieu && p.xacNhan == true && p.trangThai == false);
                if (PDK != null)
                {
                    PDK.trangThai = true;
                }
                else
                {
                    TempData["thongbao"] = "khong tim thay";
                }
                db.SaveChanges();
                //(PDK != null) ? PDK.xacNhan == true : TempData["thongbao"] = "khongtimthay";
                return RedirectToAction("Thongke");
            }
            
        }
        public ActionResult Delete(int? id)
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                var message = new MailMessage();
                var PDK = db.PhieuDangKis.FirstOrDefault(p => p.maPDK == id && p.xacNhan == false);
                if (PDK != null)
                { 
                    message.To.Add(new MailAddress(PDK.Email));
                    message.Subject = "Thông báo hủy phòng" + PDK.maPhong;
                    message.Body = "\n Bạn đã hủy phòng khách sạn T2D thành công";
                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient())
                    {
                        smtp.Send(message);
                    }
                }
                else
                {
                    TempData["thongbao"] = "khong tim thay";
                }
                db.PhieuDangKis.Remove(db.PhieuDangKis.FirstOrDefault(p => p.maPDK == id));
                db.SaveChanges();
                return RedirectToAction("Danhsachdatphong");
            }

        }
    }
}