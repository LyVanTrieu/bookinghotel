﻿using BookingHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingHotel.Controllers.Manager
{
    public class AdminController : Controller
    {
        DBConnection db = new DBConnection();
        // GET: Admin
        public ActionResult IndexAD()
        {
            //if (Session["LoginAD"] == null)
            //{
            //    return RedirectToAction("Login", "Admin");
            //}
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            var user = form["username"];
            var pass = form["password"];
            if (ModelState.IsValid)
            {
                DangNhap dn = db.DangNhaps.SingleOrDefault(x => x.userName == user && x.passWord == pass);
                if (dn != null)
                {
                    Session["LoginAD"] = dn;
                    Session["FullName"] = dn.NhanVien.tenNV.ToString();
                    Session["NV"] = dn.NhanVien;
                    return RedirectToAction("IndexAD", "Admin");
                }
                else
                    ViewBag.ThongBao = "Tên đăng nhập hoặc mật khẩu không chính xác";
            }
            return View();
        }
    }
}