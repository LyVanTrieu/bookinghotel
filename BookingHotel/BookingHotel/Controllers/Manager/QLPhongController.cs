﻿using BookingHotel.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingHotel.Controllers.Manager
{
    public class QLPhongController : Controller
    {
        // GET: Admin/QLPhong
       
        DBConnection db = new DBConnection();
        // GET: QLPhong
        public ActionResult Index()
        {
            return View();
        }
        // show Phong
        public ActionResult PhongR(int? page)
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                int pageNum = (page ?? 1);
                int pageSize = 7;
                return View(db.Phongs.ToList().Where(a => a.Status == true && a.LoaiPhong.PStatus == true).OrderBy(x => x.maPhong).ToPagedList(pageNum, pageSize));
            }
        }

        public ActionResult CreateNew()
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                Phong phong = new Phong();
                phong.listPhong = db.LoaiPhongs.Where(a => a.PStatus == true).ToList();
                return View(phong);
            }
        }
        [HttpPost]
        public ActionResult CreateNew(Phong phong, HttpPostedFileBase fileupload)
        {
            
            if (fileupload == null)
            {
                ViewBag.ThongBaoHinh = "Chọn ảnh phòng";
                return View();
            }
            else
            {
                if (ModelState.IsValid)
                {
                    var fileName = Path.GetFileName(fileupload.FileName);

                    var path = Path.Combine(Server.MapPath("~/images"), fileName);

                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = "Hình ảnh đã tồn tại";
                    }
                    else
                    {
                        fileupload.SaveAs(path);
                    }
                    phong.Image = fileName;
                    phong.tinhTrang = true;
                    phong.Status = true;
                    db.Phongs.Add(phong);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("PhongR");
        }
        // edit Phòng
        [HttpGet]
        public ActionResult EditPhongR(string id)
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                Phong phong = new Phong();
                phong = db.Phongs.SingleOrDefault(a => a.maPhong == id);
                phong.listPhong = db.LoaiPhongs.Where(a => a.PStatus == true).ToList();
                return View(phong);
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult EditPhongR(Phong phong, HttpPostedFileBase fileupload)
        {
            if (fileupload == null)
            {
                ViewBag.ThongBao = "Chọn ảnh món ăn";
                return View();
            }
            else
            {
                if (ModelState.IsValid)
                {

                    var fileName = Path.GetFileName(fileupload.FileName);

                    var path = Path.Combine(Server.MapPath("~/images"), fileName);

                    if (System.IO.File.Exists(path))
                    {
                        ViewBag.ThongBao = "Hình ảnh đã tồn tại";
                    }
                    else
                    {
                        fileupload.SaveAs(path);
                    }
                    phong.Image = fileName;
                    phong.tinhTrang = true;
                    phong.Status = true;
                    db.Entry(phong).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            return RedirectToAction("PhongR");
        }
        // xóa Phòng
        public ActionResult DeleteP(String id)
        {

            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                Phong phong = db.Phongs.SingleOrDefault(a => a.maPhong == id);
                return View(phong);
            }

        }
        [HttpPost, ActionName("DeleteP")]
        public ActionResult XacNhanXoa(string id)
        {
            Phong phong = new Phong();
            phong = db.Phongs.SingleOrDefault(a => a.maPhong == id);
            if (phong == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            phong.Status = false;
            phong.tinhTrang = false;
            db.Entry(phong).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("PhongR");
        }

    }
    }

