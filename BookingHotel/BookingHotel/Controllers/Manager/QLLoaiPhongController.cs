﻿using BookingHotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingHotel.Controllers.Manager
{
    public class QLLoaiPhongController : Controller
    {
        // GET: Admin/QLLoaiPhong
        DBConnection db = new DBConnection();
        public ActionResult Index()
        {
            return View();
        }
        // show list loại phòng
        public ActionResult LoaiPhong()
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                return View(db.LoaiPhongs.ToList().OrderBy(x => x.maLoai));
            }
        }
        // xóa và khôi phục loại món
        [HttpPost]
        public ActionResult DeletePhong(string id)
        {
            LoaiPhong phong = db.LoaiPhongs.SingleOrDefault(a => a.maLoai == id);
            if (phong == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            if (phong.PStatus == true)
            {
                phong.PStatus = false;
            }
            else
            {
                phong.PStatus = true;
            }
            db.Entry(phong).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("LoaiPhong");
        }
        // sua loại phong
        [HttpGet]
        public ActionResult EditPhong(string id)
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                LoaiPhong phong = new LoaiPhong();
                phong = db.LoaiPhongs.SingleOrDefault(a => a.maLoai == id);
                return View(phong);
            }
        }
        [HttpPost]
        public ActionResult EditPhong(LoaiPhong phong)
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                phong.PStatus = true;
                db.Entry(phong).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("LoaiPhong");
            }
        }

        // thêm loại Phòng
        [HttpGet]
        public ActionResult ThemLoaiPhong()
        {
            if (Session["LoginAD"] == null)
            {
                return RedirectToAction("Login", "Adminms");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult ThemLoaiPhong(FormCollection form)
        {
            LoaiPhong pg = new LoaiPhong();
            var maloai = form["maloai"];
            var tenloai = form["tenloai"];
            if (ModelState.IsValid)
            {
                var check = db.LoaiPhongs.SingleOrDefault(a => a.maLoai == pg.maLoai);
                var check1 = db.LoaiPhongs.SingleOrDefault(a => a.tenLoai == pg.tenLoai);
                if (check == null && check1 == null)
                {
                    pg.maLoai = maloai;
                    pg.tenLoai = tenloai;
                    pg.PStatus = true;
                    db.LoaiPhongs.Add(pg);
                    db.SaveChanges();
                    return RedirectToAction("LoaiPhong", "QLLoaiPhong");
                }
                else
                {
                    ViewBag.Error = "Mã Loại phòng hoặc tên loại phòng đã tồn tại";
                    return View();
                }
            }
            return View();
        }
    }
}