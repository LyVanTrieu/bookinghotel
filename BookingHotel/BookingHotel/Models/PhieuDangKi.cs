namespace BookingHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhieuDangKi")]
    public partial class PhieuDangKi
    {
        [Key]
        public int maPDK { get; set; }

        public int? maKH { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ngayLap { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ngayDi { get; set; }

        [StringLength(4)]
        public string maPhong { get; set; }

        public int? traTruoc { get; set; }

        [StringLength(10)]
        public string soNguoi { get; set; }

        public int? Tongtien { get; set; }

        [StringLength(4)]
        public string maNV { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ngayNhan { get; set; }

        public bool? trangThai { get; set; }

        public bool? xacNhan { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public virtual KhachHang KhachHang { get; set; }

        public virtual NhanVien NhanVien { get; set; }

        public virtual Phong Phong { get; set; }
    }
}
