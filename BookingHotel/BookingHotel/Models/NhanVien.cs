namespace BookingHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NhanVien")]
    public partial class NhanVien
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public NhanVien()
        {
            DangNhaps = new HashSet<DangNhap>();
            PhieuDangKis = new HashSet<PhieuDangKi>();
        }

        [Key]
        [StringLength(4)]
        public string maNV { get; set; }

        [StringLength(100)]
        public string tenNV { get; set; }

        [StringLength(4)]
        public string maCV { get; set; }

        [StringLength(3)]
        public string gioiTinh { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? ngaySinh { get; set; }

        [StringLength(100)]
        public string diaChi { get; set; }

        [StringLength(13)]
        public string Sdt { get; set; }

        public virtual ChucVu ChucVu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DangNhap> DangNhaps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhieuDangKi> PhieuDangKis { get; set; }
    }
}
