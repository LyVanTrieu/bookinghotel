namespace BookingHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Phong")]
    public partial class Phong
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Phong()
        {
            PhieuDangKis = new HashSet<PhieuDangKi>();
        }

        [Key]
        [StringLength(4)]
        public string maPhong { get; set; }

        [StringLength(100)]
        public string tenPhong { get; set; }

        public bool? tinhTrang { get; set; }

        [StringLength(4)]
        public string maLoai { get; set; }

        public int? giaPhong { get; set; }

        public bool? Status { get; set; }

        public string Image { get; set; }

        public string Describe { get; set; }

        public virtual LoaiPhong LoaiPhong { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhieuDangKi> PhieuDangKis { get; set; }
        public List<LoaiPhong> listPhong = new List<LoaiPhong>();
    }
}
