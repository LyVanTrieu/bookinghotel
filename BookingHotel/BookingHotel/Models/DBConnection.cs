namespace BookingHotel.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBConnection : DbContext
    {
        public DBConnection()
            : base("name=DBConnection")
        {
        }

        public virtual DbSet<ChucVu> ChucVus { get; set; }
        public virtual DbSet<DangNhap> DangNhaps { get; set; }
        public virtual DbSet<KhachHang> KhachHangs { get; set; }
        public virtual DbSet<LoaiPhong> LoaiPhongs { get; set; }
        public virtual DbSet<NhanVien> NhanViens { get; set; }
        public virtual DbSet<PhieuDangKi> PhieuDangKis { get; set; }
        public virtual DbSet<Phong> Phongs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChucVu>()
                .Property(e => e.maCV)
                .IsUnicode(false);

            modelBuilder.Entity<DangNhap>()
                .Property(e => e.userName)
                .IsUnicode(false);

            modelBuilder.Entity<DangNhap>()
                .Property(e => e.passWord)
                .IsUnicode(false);

            modelBuilder.Entity<DangNhap>()
                .Property(e => e.maCV)
                .IsUnicode(false);

            modelBuilder.Entity<DangNhap>()
                .Property(e => e.maNV)
                .IsUnicode(false);

            modelBuilder.Entity<KhachHang>()
                .Property(e => e.Sdt)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<LoaiPhong>()
                .Property(e => e.maLoai)
                .IsUnicode(false);

            modelBuilder.Entity<NhanVien>()
                .Property(e => e.maNV)
                .IsUnicode(false);

            modelBuilder.Entity<NhanVien>()
                .Property(e => e.maCV)
                .IsUnicode(false);

            modelBuilder.Entity<NhanVien>()
                .Property(e => e.Sdt)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PhieuDangKi>()
                .Property(e => e.maPhong)
                .IsUnicode(false);

            modelBuilder.Entity<PhieuDangKi>()
                .Property(e => e.soNguoi)
                .IsUnicode(false);

            modelBuilder.Entity<PhieuDangKi>()
                .Property(e => e.maNV)
                .IsUnicode(false);

            modelBuilder.Entity<Phong>()
                .Property(e => e.maPhong)
                .IsUnicode(false);

            modelBuilder.Entity<Phong>()
                .Property(e => e.maLoai)
                .IsUnicode(false);
        }
    }
}
