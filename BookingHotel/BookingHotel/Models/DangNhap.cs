namespace BookingHotel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DangNhap")]
    public partial class DangNhap
    {
        [Key]
        [StringLength(50)]
        public string userName { get; set; }

        [StringLength(50)]
        public string passWord { get; set; }

        [StringLength(4)]
        public string maCV { get; set; }

        [StringLength(4)]
        public string maNV { get; set; }

        public virtual NhanVien NhanVien { get; set; }
    }
}
