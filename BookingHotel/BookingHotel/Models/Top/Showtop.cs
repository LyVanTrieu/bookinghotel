﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingHotel.Models.Top
{

    public class Showtop
    {
        public static IEnumerable<Phong> Top4()
        {
            var db = new DBConnection();

            return db.Phongs.SqlQuery("select top(3)* from Phong");
        }
       //top2 room
        public static IEnumerable<Phong> Top2()
        {
            var db = new DBConnection();

            return db.Phongs.SqlQuery("select top(2)* from Phong");
        }
    }
}